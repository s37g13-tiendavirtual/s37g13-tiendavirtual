<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP - Hello World</title>
    </head>
    <body>
        <br/>
        <h1><%= "Hello World!" %>
        </h1>
        <br>
        <h2>
            Hola Tripulantes
        </h2>
        <br/>
        <a href="hello-servlet">Hello Servlet</a>
        <br>
        <a href="gitlab.com">Ingresa a la Página de GitLab</a>
        <br>
        <p>
            HTML (Lenguaje de Marcas de Hipertexto,
            del inglés HyperText Markup Language) es el
            componente más básico de la Web. Define el significado
            y la estructura del contenido web.
        </p>
    </body>
</html>