package com.tiendaonline.proyjavatiendaonline;

import java.io.*;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello Tripulantes";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        for (int i = 0; i < 6; i++) {
            out.println("<h" + (i+1) + ">" + "Etiquita en h" + (i+1) + ", Variable i = " + i + "</h" + (i+1) + ">");
        }
        out.println("</body></html>");
    }

    public void destroy() {
    }
}